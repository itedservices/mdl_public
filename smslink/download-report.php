<?php

require_once('../config.php');
require_once('renderer.php');

$year = required_param('year', PARAM_ALPHANUMEXT);
$term = required_param('term', PARAM_ALPHANUMEXT);
$filename = required_param('filename', PARAM_ALPHANUMEXT);

// KCI setup
global $CFG, $USER;
require_once($CFG->libdir."/kamar/kamarcommoninterface/kamar.php");
require_once($CFG->libdir."/kamar/kamarcommondisplay/kamar.php");

$kamar = Kamar::getKAMARInstance();
$kamar->setFMPath($CFG->block_kamar_server_address);
$kamar->setWebAPIPassword($CFG->block_kamar_web_api_password);
$student_block_version = 'versionunknown';
$blocks = $DB->get_records('block', array('name' => 'kamar_students'));
foreach ($blocks as $block) {
    if ($block->name == 'kamar_students') {
		$student_block_version = $block->version;
    }
}
$kamar_cache = KamarCache::getKAMARCacheInstance();
$kamar_cache->setCachePath($CFG->block_kamar_cache_directory, 'KAMARMoodle-' . $student_block_version);
$kcd_utilities = KamarKCDUtilities::getKamarKCDUtilitiesInstance();
$kcd_utilities->setBaseURL($CFG->wwwroot . '/smslink/');
	
$student = StudentFactory::buildStudentFromUsername($USER->username, date('Y'));
$reports_path = $CFG->block_kamar_reports_directory;

// validate the student downloading the report is the student the report belongs to
$parts = explode('_', $filename);
if($parts[0] == $student->student_id)
{
	$file_path = $reports_path . $year . DIRECTORY_SEPARATOR . $term . DIRECTORY_SEPARATOR . $filename . '.pdf';
	if(!is_file($file_path))
		die('Can Not Find File: '. $file_path);
	header("Content-type: application/force-download");
	header("Content-Transfer-Encoding: Binary");
	header("Content-length: " . filesize($file_path));
	header("Content-disposition: attachment; filename=\"" . $filename . ' ' . $term . ".pdf\"");
	readfile($file_path);
	die();
}

// failed to download report, build a page to show
// moodle setup page stuff
$smslink_css = new moodle_url('/smslink/styles.css');
$PAGE->requires->css($smslink_css, true);

$PAGE->set_title('SMS Link - Report Download Failed');
$PAGE->set_heading('SMS Link');
$PAGE->set_pagelayout('smslink');
echo $OUTPUT->header();
echo html_writer::tag('p', 'There was a problem downloading the report.');
echo $OUTPUT->footer();