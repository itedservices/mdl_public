<?php

class smslink_notices implements renderable
{
	public $selected_date;
	public $earliest_date;
	public $latest_date;
	public $notice_list;
	
	public function __construct($new_selected_date, $new_notice_list)
	{
		$this->selected_date = $new_selected_date;
		$this->earliest_date = mktime(0, 0, 0, 1, 1, date('Y') - 1);
		$this->latest_date = mktime(0, 0, 0, 12, 31, date('Y') + 1);
		$this->notice_list = $new_notice_list;
	}
}

class smslink_events implements renderable
{
	public $selected_date;
	public $earliest_date;
	public $latest_date;
	public $event_list;
	public $range;
	
	public function __construct($new_selected_date, $new_event_list, $new_range)
	{
		$this->selected_date = $new_selected_date;
		$this->earliest_date = mktime(0, 0, 0, 1, 1, date('Y') - 1);
		$this->latest_date = mktime(0, 0, 0, 12, 31, date('Y') + 1);
		$this->event_list = $new_event_list;
		$this->range = $new_range;
	}
}

class smslink_details implements renderable
{
	public $student;
	public $kamaradminemail;
	public $post_url;
	
	public function __construct($new_student, $new_kamaradminemail, $new_post_url)
	{
		$this->student = $new_student;
		$this->kamaradminemail = $new_kamaradminemail;
		$this->post_url = $new_post_url;
	}
}

class smslink_timetable implements renderable
{
	public $selected_week;
	public $student;
	public $student_timetable;
	
	public function __construct($new_selected_week, $new_student, $new_student_timetable)
	{
		$this->selected_week = $new_selected_week;
		$this->student = $new_student;
		$this->student_timetable = $new_student_timetable;
	}
}

class smslink_attendance extends smslink_timetable implements renderable
{
	public $student_attendance;
	public $attendance_delay;
	public $codes_to_hide;
	
	public function __construct($new_selected_week, $new_student, $new_student_timetable, $new_student_attendance, $new_attendance_delay, $new_codes_to_hide)
	{
		parent::__construct($new_selected_week, $new_student, $new_student_timetable);
		
		$this->student_attendance = $new_student_attendance;
		$this->attendance_delay = $new_attendance_delay;
		$this->codes_to_hide = $new_codes_to_hide;
	}
}

class smslink_results implements renderable
{
	public $student;
	public $student_result_list;
	public $assessment_type_codes;
    public $assessment_comment_codes;
	public $enrolled_only;
	public $published_only;
	
	public function __construct($new_student, $new_student_result_list, $new_assessment_type_codes, $new_assessment_comment_codes, $new_enrolled_only, $new_published_only)
	{
		$this->student = $new_student;
		$this->student_result_list = $new_student_result_list;
		$this->assessment_type_codes = $new_assessment_type_codes;
        $this->assessment_comment_codes = $new_assessment_comment_codes;
		$this->enrolled_only = $new_enrolled_only;
		$this->published_only = $new_published_only;
	}
}

class smslink_results_nzqa extends smslink_results implements renderable
{
	public $ncea_summary;
	public $graph_url;
	
	public function __construct($new_student, $new_student_result_list, $new_assessment_type_codes, $new_ncea_summary, $new_enrolled_only, $new_published_only, $new_graph_url)
	{
		parent::__construct($new_student, $new_student_result_list, $new_assessment_type_codes, $new_assessment_comment_codes, $new_enrolled_only, $new_published_only);
		
		$this->ncea_summary = $new_ncea_summary;
		$this->graph_url = $new_graph_url;
	}
}

class smslink_results_subject extends smslink_results implements renderable
{
	public $year_to_show;
	
	public function __construct($new_student, $new_student_result_list, $new_assessment_type_codes, $new_assessment_comment_codes, $new_enrolled_only, $new_published_only, $new_year_to_show)
	{
		parent::__construct($new_student, $new_student_result_list, $new_assessment_type_codes, $new_assessment_comment_codes, $new_enrolled_only, $new_published_only);
		
		$this->year_to_show = $new_year_to_show;
	}
}

class smslink_results_all extends smslink_results implements renderable
{
}

class smslink_results_nceagraph implements renderable
{
	public $student;
	public $ncea_summary;
	
	public function __construct($new_student, $new_ncea_summary)
	{
		$this->student = $new_student;
		$this->ncea_summary = $new_ncea_summary;
	}
}

class smslink_groups implements renderable
{
	public $student;
	public $student_group_list;
	public $show_groups_from_year;
	
	public function __construct($new_student, $new_student_group_list, $new_show_groups_from_year)
	{
		$this->student = $new_student;
		$this->student_group_list = $new_student_group_list;
		$this->show_groups_from_year = $new_show_groups_from_year;
	}
}

class smslink_awards implements renderable
{
	public $student;
	public $student_award_list;
	
	public function __construct($new_student, $new_student_award_list)
	{
		$this->student = $new_student;
		$this->student_award_list = $new_student_award_list;
	}
}

class smslink_pastoral implements renderable
{
	public $student;
	public $student_pastoral_list;
	
	public function __construct($new_student, $new_student_pastoral_list)
	{
		$this->student = $new_student;
		$this->student_pastoral_list = $new_student_pastoral_list;
	}
}

class smslink_reports implements renderable
{
	public $student;
	public $reports_path;
	
	public function __construct($new_student, $new_reports_path)
	{
		$this->student = $new_student;
		$this->reports_path = $new_reports_path;
	}
}

class smslink_fees implements renderable
{
	public $student;
	public $fee_payer;
	public $fee_summary;
	public $year_to_show;
	public $show_credit_balance;
	public $show_carry_forward_charges;
	
	public function __construct($new_student, $new_fee_payer, $new_fee_summary, $new_year_to_show, $new_show_credit_balance, $new_show_carry_forward_charges)
	{
		$this->student = $new_student;
		$this->fee_payer = $new_fee_payer;
		$this->fee_summary = $new_fee_summary;
		$this->year_to_show = $new_year_to_show;
		$this->show_credit_balance = $new_show_credit_balance;
		$this->show_carry_forward_charges = $new_show_carry_forward_charges;
	}
}

class smslink_careers implements renderable
{
	public $student;
	public $careers_fields_enabled;
	public $careers_headings;
	
	public function __construct($new_student, $new_careers_fields_enabled, $new_careers_headings)
	{
		$this->student = $new_student;
		$this->careers_fields_enabled = $new_careers_fields_enabled;
		$this->careers_headings = $new_careers_headings;
	}
}

class smslink_interviews implements renderable
{
	public $student;
	public $interviewlist;
	public $viewing_interview;
	public $viewing_interview_answer_list;
	public $viewing_interview_is_read_only;
	public $post_url;
	
	public function __construct($new_student, $new_interviewlist, $new_viewing_interview, $new_viewing_interview_answer_list, $new_viewing_interview_is_read_only, $new_post_url)
	{
		$this->student = $new_student;
		$this->interviewlist = $new_interviewlist;
		$this->viewing_interview = $new_viewing_interview;
		$this->viewing_interview_answer_list = $new_viewing_interview_answer_list;
		$this->viewing_interview_is_read_only = $new_viewing_interview_is_read_only;
		$this->post_url = $new_post_url;
	}
}

class smslink_course_selection implements renderable
{
	public $student;
	public $selection_year;
	public $selection_year_options;
	public $course_selection_xml;
	public $url_status;
	public $post_url;
	
	public function __construct($new_student, $new_selection_year, $new_selection_year_options, $new_course_selection_xml, $new_url_status, $new_post_url)
	{
		$this->student = $new_student;
		$this->selection_year = $new_selection_year;
		$this->selection_year_options = $new_selection_year_options;
		$this->course_selection_xml = $new_course_selection_xml;
		$this->url_status = $new_url_status;
		$this->post_url = $new_post_url;
	}
}

class smslink_library implements renderable
{
	public $student;
	public $raw_xml;
	public $valid_query;
	
	public function __construct($new_student, $new_raw_xml, $new_valid_query)
	{
		$this->student = $new_student;
		$this->raw_xml = $new_raw_xml;
		$this->valid_query = $new_valid_query;
	}
}

class smslink_enrol implements renderable
{
	public $student;
	public $enrolled_moodle_courses;
	public $removed_moodle_courses;
	public $failed_moodle_courses;
	
	public function __construct($new_student, $new_enrolled_moodle_courses, $new_removed_moodle_courses, $new_failed_moodle_courses)
	{
		$this->student = $new_student;
		$this->enrolled_moodle_courses = $new_enrolled_moodle_courses;
		$this->removed_moodle_courses = $new_removed_moodle_courses;
		$this->failed_moodle_courses = $new_failed_moodle_courses;
	}
}

class mod_smslink_renderer extends plugin_renderer_base
{
	protected function render_smslink_notices(smslink_notices $notice_list_wrapper)
	{
		$previous_day_url = new moodle_url('/smslink/index.php', array('view' => 'notices', 'date' => date("Y-m-d", strtotime('-1 day', $notice_list_wrapper->selected_date))));
		$next_day_url = new moodle_url('/smslink/index.php', array('view' => 'notices', 'date' => date("Y-m-d", strtotime('+1 day', $notice_list_wrapper->selected_date))));
		$today_url = new moodle_url('/smslink/index.php', array('view' => 'notices'));
		
		// build page
		if ($notice_list_wrapper->selected_date > $notice_list_wrapper->earliest_date)
			$links = $this->output->container($this->output->action_link($previous_day_url, '&laquo; View Previous Day\'s Notices'), 'notice-control');
		else
			$links = $this->output->container(html_writer::tag('span', '&laquo; View Previous Day\'s Notices'), 'notice-control');
		$links .= $this->output->container($this->output->action_link($today_url, 'View Today\'s Notices'), 'notice-control');
		if ($notice_list_wrapper->selected_date < $notice_list_wrapper->latest_date)
			$links .= $this->output->container($this->output->action_link($next_day_url, 'View Next Day\'s Notices &raquo;'), 'notice-control');
		else
			$links .= $this->output->container(html_writer::tag('span', 'View Next Day\'s Notices &raquo;'), 'notice-control');
		
		// attach navigation area
		$out = $this->output->container($links, 'notice-controls');
		// attach notice list
		$out .= NoticeDisplay::showNotices($notice_list_wrapper->selected_date, $notice_list_wrapper->notice_list);
		
		return $out;
	}
	
	protected function render_smslink_events(smslink_events $event_list_wrapper)
	{
		$previous_day_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'date' => date("Y-m-d", strtotime('-1 month', $event_list_wrapper->selected_date)), 'range' => $event_list_wrapper->range));
		$next_day_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'date' => date("Y-m-d", strtotime('+1 month', $event_list_wrapper->selected_date)), 'range' => $event_list_wrapper->range));
		$today_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'range' => $event_list_wrapper->range));
		
		// build page
		$links = '';
		if ($event_list_wrapper->selected_date > $event_list_wrapper->earliest_date)
			$links = $this->output->container($this->output->action_link($previous_day_url, html_writer::tag('img', '', array('src' => new moodle_url('/smslink/assets/images/LeftArrowBlack.png'), 'title' => 'Show previous month'))), 'backamontharrow');
		$links .= $this->output->container($this->output->action_link($today_url, date("F Y", $event_list_wrapper->selected_date)), 'month-status');
		if ($event_list_wrapper->selected_date < $event_list_wrapper->latest_date)
			$links .= $this->output->container($this->output->action_link($next_day_url, html_writer::tag('img', '', array('src' => new moodle_url('/smslink/assets/images/RightArrowBlack.png'), 'title' => 'Show next month'))), 'forwardamontharrow');
		
		// attach navigation area
		$out = $this->output->container($links, 'month-details');
		// attach date picker
		$calendar_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'date' => date('Y-n', $event_list_wrapper->selected_date)));
		$out .= EventsDisplay::showCalendarPicker($event_list_wrapper->event_list, $event_list_wrapper->selected_date, $calendar_url, $event_list_wrapper->range);
		
		$month_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'date' => date("Y-m-d", $event_list_wrapper->selected_date), 'range' => 'month'));
		$week_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'date' => date("Y-m-d", $event_list_wrapper->selected_date), 'range' => 'week'));
		$day_url = new moodle_url('/smslink/index.php', array('view' => 'calendar', 'date' => date("Y-m-d", $event_list_wrapper->selected_date), 'range' => 'day'));
	
		$month_link = (($event_list_wrapper->range == 'month') ? 'Month' : $this->output->action_link($month_url, 'Month'));
		$week_link = (($event_list_wrapper->range == 'week') ? 'Week' : $this->output->action_link($week_url, 'Week'));
		$day_link = (($event_list_wrapper->range == 'day') ? 'Day' : $this->output->action_link($day_url, 'Day'));
		
		$range_links = '';
		$range_links .= html_writer::tag('p', 'View Event Range by:');
		$range_links .= html_writer::tag('span', $month_link, array('class' => (($event_list_wrapper->range == 'month') ? 'current' : '')));
		$range_links .= html_writer::tag('span', $week_link, array('class' => (($event_list_wrapper->range == 'week') ? 'current' : '')));
		$range_links .= html_writer::tag('span', $day_link, array('class' => (($event_list_wrapper->range == 'day') ? 'current' : '')));
		
		$out .= $this->output->container($range_links, 'range-selector');
		
		// attach event list
		if ($event_list_wrapper->range == 'day')
			$out .= EventsDisplay::showEventsForDate($event_list_wrapper->event_list, $event_list_wrapper->selected_date);
		elseif ($event_list_wrapper->range == 'week')
			$out .= EventsDisplay::showEventsForWeek($event_list_wrapper->event_list, $event_list_wrapper->selected_date);
		else
			$out .= EventsDisplay::showEvents($event_list_wrapper->event_list);
		
		return $out;
	}
	
	protected function render_smslink_details(smslink_details $details_render_wrapper)
	{
		$out =<<<EOT
<script type="text/javascript" charset="utf-8">
	function $(id) { return document.getElementById(id) }
	function swapTab(show) {
		if($('tab_student'))            { $('tab_student').className = 'tab-swap-hidden'; } 
		if($('tab_caregivers'))         { $('tab_caregivers').className = 'tab-swap-hidden'; }
		if($('tab_emergency'))          { $('tab_emergency').className = 'tab-swap-hidden'; }
		if($('tab_medical'))            { $('tab_medical').className = 'tab-swap-hidden'; }
		if($('tab_change'))             { $('tab_change').className = 'tab-swap-hidden'; }
		$(show).className = '';
		return false;
	}
</script>
EOT;
	
		// variables to confirm if certian details should be blocked
		$caregiver_details_blocked = false;
		// check if details screen should be limited based on user flags
		if ($details_render_wrapper->student->caregiver_access_none || $details_render_wrapper->student->caregiver_access_limited)
			$caregiver_details_blocked = true;

		$menulist = array();
		$menulist[] = html_writer::link(new moodle_url('/smslink/index.php', array('view' => 'details', 'section' => 'student')), 'Student', array('class' => 'tab', 'onclick' => "return swapTab('tab_student');"));
		if (!$caregiver_details_blocked)
		{
			$menulist[] = html_writer::link(new moodle_url('/smslink/index.php', array('view' => 'details', 'section' => 'caregivers')), 'Caregivers', array('class' => 'tab', 'onclick' => "return swapTab('tab_caregivers');"));
			$menulist[] = html_writer::link(new moodle_url('/smslink/index.php', array('view' => 'details', 'section' => 'emergency')), 'Emergency Contact', array('class' => 'tab', 'onclick' => "return swapTab('tab_emergency');"));
			$menulist[] = html_writer::link(new moodle_url('/smslink/index.php', array('view' => 'details', 'section' => 'medical')), 'Medical', array('class' => 'tab', 'onclick' => "return swapTab('tab_medical');"));
		}
		if (!empty($details_render_wrapper->kamaradminemail))
			$menulist[] = html_writer::link(new moodle_url('/smslink/index.php', array('view' => 'details', 'section' => 'change')), 'Change of Details', array('class' => 'tab', 'onclick' => "return swapTab('tab_change');"));
		// Display the content as a list
		$out .= $this->output->container(html_writer::alist($menulist, array('class'=>'list')), 'tab-swap-nav', '');
		
		$out .= $this->output->container($this->output->heading('Student Details', 2, '') . DetailsDisplay::showCoreDetails($details_render_wrapper->student, $caregiver_details_blocked), '', 'tab_student');
		if (!$caregiver_details_blocked)
		{
			$out .= $this->output->container($this->output->heading('Student Caregiver Details', 2, '') . DetailsDisplay::showPrimaryCaregiverDetails($details_render_wrapper->student) . DetailsDisplay::showSecondaryCaregiverDetails($details_render_wrapper->student), 'tab-swap-hidden', 'tab_caregivers');
			$out .= $this->output->container($this->output->heading('Student Emergency Contact Details', 2, '') . DetailsDisplay::showEmergencyContactDetails($details_render_wrapper->student), 'tab-swap-hidden', 'tab_emergency');
			$out .= $this->output->container($this->output->heading('Student Medical Details', 2, '') . DetailsDisplay::showMedicalDetails($details_render_wrapper->student), 'tab-swap-hidden', 'tab_medical');
		}
		$out .= $this->output->container($this->output->heading('Change Details', 2, '') . DetailsDisplay::showChangeOfDetailsRequest($details_render_wrapper->student, $details_render_wrapper->post_url), 'tab-swap-hidden', 'tab_change');
		
		return $out;
	}
	
	protected function render_smslink_timetable(smslink_timetable $timetable_render_wrapper)
	{
		$url_last_week = new moodle_url('/smslink/index.php', array('view' => 'timetable', 'week' => $timetable_render_wrapper->selected_week - 1));
		$url_next_week = new moodle_url('/smslink/index.php', array('view' => 'timetable', 'week' => $timetable_render_wrapper->selected_week + 1));
	
		$out = TimetableDisplay::showTimetableForStudent($timetable_render_wrapper->student, $timetable_render_wrapper->student_timetable, $timetable_render_wrapper->selected_week, $url_last_week, $url_next_week);
		
		return $out;
	}
	
	protected function render_smslink_attendance(smslink_timetable $attendance_render_wrapper)
	{
		$url_last_week = new moodle_url('/smslink/index.php', array('view' => 'attendance', 'week' => $attendance_render_wrapper->selected_week - 1));
		$url_next_week = new moodle_url('/smslink/index.php', array('view' => 'attendance', 'week' => $attendance_render_wrapper->selected_week + 1));
	
		$out = AttendanceDisplay::showAttendanceTimetableForStudent($attendance_render_wrapper->student, $attendance_render_wrapper->student_timetable, $attendance_render_wrapper->selected_week, $url_last_week, $url_next_week, $attendance_render_wrapper->student_attendance, $attendance_render_wrapper->attendance_delay, $attendance_render_wrapper->codes_to_hide);
		$out .= AttendanceDisplay::showAttendanceTimetableKey($attendance_render_wrapper->codes_to_hide);
		
		return $out;
	}
	
	protected function render_smslink_results_nzqa(smslink_results_nzqa $result_list_render_wrapper)
	{
		$out = $this->output->heading('NZQA Ratified Results', 2, '', '');
		$out .= ResultDisplay::showNCEASummaryRatifiedResults($result_list_render_wrapper->student, $result_list_render_wrapper->ncea_summary);
		if (!empty($ncea_summary->nzqa_ratified['ratified_timestamp']))
			$out .= html_writer::tag('p', '* The above results have been received by the school from NZQA on the ' . date('jS F Y', $ncea_summary->nzqa_ratified['ratified_timestamp']) . ', these should properly reflect NZQA\'s official records at the time they were added.');
		else
			$out .= html_writer::tag('p', '* There are no official results from NZQA available yet.');
		$out .= $this->output->heading('Stored Results - Summarised', 2, '', '');
		$out .= ResultDisplay::showNCEASummaryStoredResults($result_list_render_wrapper->student, $result_list_render_wrapper->ncea_summary, $result_list_render_wrapper->graph_url);
		$out .= html_writer::tag('p', '* Stored results may not be confirmed with NZQA and should not be considered official.');
		$out .= $this->output->heading('Stored Results - Detailed', 2, '', '');
		$out .= ResultDisplay::showAllResults($result_list_render_wrapper->student, $result_list_render_wrapper->student_result_list, $result_list_render_wrapper->assessment_type_codes, $result_list_render_wrapper->enrolled_only, $result_list_render_wrapper->published_only);
		
		return $out;
	}
	
	protected function render_smslink_results_subject(smslink_results_subject $result_list_render_wrapper)
	{
		$out = ResultDisplay::showSubjectResultsForYear($result_list_render_wrapper->student, $result_list_render_wrapper->student_result_list, $result_list_render_wrapper->assessment_type_codes, $result_list_render_wrapper->assessment_comment_codes, $result_list_render_wrapper->enrolled_only, $result_list_render_wrapper->published_only, $result_list_render_wrapper->year_to_show);
		
		return $out;
	}
	
	protected function render_smslink_results_all(smslink_results_all $result_list_render_wrapper)
	{
		$out = ResultDisplay::showAllResults($result_list_render_wrapper->student, $result_list_render_wrapper->student_result_list, $result_list_render_wrapper->assessment_type_codes, $result_list_render_wrapper->assessment_comment_codes, $result_list_render_wrapper->enrolled_only, $result_list_render_wrapper->published_only);
		
		return $out;
	}
	
	protected function render_smslink_results_nceagraph(smslink_results_nceagraph $result_graph_render_wrapper)
	{
		ob_start();
			$result_graph_render_wrapper->ncea_summary->graph->renderToOutput( 300, 170 );
		return ob_get_clean();
	}
	
	protected function render_smslink_groups(smslink_groups $group_list_render_wrapper)
	{
		$out = GroupDisplay::showGroupsForYear($group_list_render_wrapper->student, $group_list_render_wrapper->student_group_list, $group_list_render_wrapper->show_groups_from_year);
		
		return $out;
	}
	
	protected function render_smslink_awards(smslink_awards $award_list_render_wrapper)
	{
		$out = AwardDisplay::showAwards($award_list_render_wrapper->student, $award_list_render_wrapper->student_award_list);
		
		return $out;
	}
	
	protected function render_smslink_pastoral(smslink_pastoral $pastoral_list_render_wrapper)
	{
		$out = PastoralDisplay::showPastoralEvents($pastoral_list_render_wrapper->student, $pastoral_list_render_wrapper->student_pastoral_list);
		
		return $out;
	}
	
	protected function render_smslink_reports(smslink_reports $reports_render_wrapper)
	{
		$out = '';
		
		$base_reports_path = $reports_render_wrapper->reports_path;
		$years_paths = glob($base_reports_path . '*');
		if(is_array($years_paths))
		{
			// sort into year numeric order
			array_multisort($years_paths, SORT_DESC);
			// loop over each year directory
			foreach($years_paths as $year_path)
			{
				// grab just the year from the end of the path
				$year = substr($year_path, strlen($base_reports_path));
				// check if the directory name is just a year number
				if(is_numeric($year))
				{
					// output the year heading
					$out .= $this->output->heading($year, 2, '', '');
					
					$terms_path = glob($year_path . DIRECTORY_SEPARATOR . '*');
					if(is_array($terms_path))
					{
						// sort the terms alphabetically
						array_multisort($terms_path, SORT_ASC);
						// loop over each term directory
						foreach($terms_path as $term_path)
						{
							// grab just the term from the end of the path
							$term = substr($term_path, strlen($year_path) + 1);
				
							$report_paths = glob($term_path . DIRECTORY_SEPARATOR . $reports_render_wrapper->student->student_id . '_*');
							
							// check that there is a valid report
							if (is_array($report_paths) && !empty($report_paths) && is_file($report_paths[0]))
							{
								$filename = substr($report_paths[0], strlen($term_path) + 1);
								$report_title = str_replace('_', ' ', $term) . ' report';
								$link_to_report = new moodle_url('/smslink/download-report.php', array('year' => $year, 'term' => $term, 'filename' => str_replace('.pdf', '', $filename)));
								$out .= html_writer::link($link_to_report, $report_title);
							}
							else
							{
								$out .= html_writer::tag('p', 'There was no report found for ' . $reports_render_wrapper->student->preferred_first_name . '.');
							}
						}
					}
					else
					{
						$out .= html_writer::tag('p', 'There were no reports found for this year.');
					}
				}
			}
		}
		else
		{
			$out .= html_writer::tag('p', 'There were no reports found.');
		}
		
		return $out;
	}
	
	protected function render_smslink_fees(smslink_fees $fees_render_wrapper)
	{
		// fees records missing
		if ($fees_render_wrapper->fee_payer == null || $fees_render_wrapper->fee_summary == null)
		{
			$out = html_writer::tag('p', 'Fees are not enabled for ' . $fees_render_wrapper->student->preferred_first_name . '.');
		}
		else
		{
			if ($fees_render_wrapper->student->caregiver_access_none || $fees_render_wrapper->student->caregiver_access_limited)
			{
				$out = html_writer::tag('p', 'This page is not viewable by your account.');
			}
			else
			{
				$out = FeeDisplay::showFees($fees_render_wrapper->student, $fees_render_wrapper->fee_payer, $fees_render_wrapper->fee_summary, $fees_render_wrapper->year_to_show, $fees_render_wrapper->show_credit_balance, $fees_render_wrapper->show_carry_forward_charges);
			}
		}
		
		return $out;
	}
	
	protected function render_smslink_careers(smslink_careers $careers_render_wrapper)
	{
		$out = CareerDisplay::showCareersInformation($careers_render_wrapper->student, $careers_render_wrapper->careers_fields_enabled, $careers_render_wrapper->careers_headings);
		
		return $out;
	}
	
	protected function render_smslink_interviews(smslink_interviews $careers_render_wrapper)
	{
		if (empty($careers_render_wrapper->interviewlist) || empty($careers_render_wrapper->interviewlist->interviews))
		{
			$out = html_writer::tag('p', 'You have no interviews to display.');
		}
		else
		{
			$interview_list_html = '';
			foreach ($careers_render_wrapper->interviewlist->interviews as $interview)
			{
				if ($interview->interview_id == $careers_render_wrapper->viewing_interview->interview_id)
					$viewing_interview = $interview;
				
				$interview_link = html_writer::tag('span', html_writer::link(new moodle_url('/smslink/index.php', array('view' => 'interviews', 'viewing' => $interview->interview_id)), $interview->purpose), array('class' => 'interview-title'));
				$interview_date = html_writer::tag('span', date('M Y', $interview->interview_date_timestamp), array('class' => 'interview-date'));
				$interview_teacher = html_writer::tag('span', $interview->teacher, array('class' => 'interview-teacher'));
				
				$interview_list_html .= $this->output->container($interview_link . $interview_date . ' - ' . $interview_teacher, 'interview' . (($viewing_interview == $interview) ? ' viewing-interview' : ''));
			}
			
			$out = $this->output->container($interview_list_html, '', 'interview-list');
			
			if ($careers_render_wrapper->viewing_interview !== null)
			{
				$out .= CareerDisplay::showInterviews($careers_render_wrapper->student, $careers_render_wrapper->interviewlist, $careers_render_wrapper->viewing_interview, $careers_render_wrapper->viewing_interview_answer_list, $careers_render_wrapper->viewing_interview_is_read_only, $careers_render_wrapper->post_url);
			}
		}
		
		return $out;
	}
	
	protected function render_smslink_course_selection(smslink_course_selection $course_selection_render_wrapper)
	{
		if (!empty($course_selection_render_wrapper->url_status))
		{
			if ($course_selection_render_wrapper->url_status == 'success')
				$out = $this->output->container('Your subject selections have been saved', 'notifysuccess');
			else if ($course_selection_render_wrapper->url_status == 'error')
				$out = $this->output->container('There was a problem saving your option selections, please try again and if the issue continues contact the school', 'notifyproblem error');
		}
		else if ($course_selection_render_wrapper->selection_year_options == null)
		{
			$out = html_writer::tag('p', 'Course selection is enabled but doesn\'t appear to be correctly set up for your ' . $course_selection_render_wrapper->selection_year . ' timetable year. Please contact the school and quote this message to have this addressed.');
		}
		else if ($course_selection_render_wrapper->course_selection_xml !== false)
		{
			$out = $this->output->heading('Selecting Courses for Year ' . $course_selection_render_wrapper->selection_year_options->year_level . ' [' . $course_selection_render_wrapper->selection_year . ']', 2, '');
			$out .= CourseSelectionDisplay::showCourseSelectionOptions($course_selection_render_wrapper->student, $course_selection_render_wrapper->selection_year_options, $course_selection_render_wrapper->course_selection_xml, $course_selection_render_wrapper->post_url);
		}
		else
		{
			$out = html_writer::tag('p', 'Course selection is enabled but doesn\'t appear to be correctly set up. Please contact the school and quote this message to have this addressed.');
		}
		
		return $out;
	}
	
	protected function render_smslink_library(smslink_library $library_render_wrapper)
	{
		if ($library_render_wrapper->valid_query):
			// appears we need to do this to fix the namespace? not sure why - Adam
			$raw_xml = str_replace('xmlns=', 'ns=', $library_render_wrapper->raw_xml); //$raw_xml is a string that contains xml...
			$xml = new SimpleXMLElement($raw_xml);
			$issues_xml = $xml->xpath('/FMPXMLRESULT/RESULTSET/ROW');
			
			$out = LibraryDisplay::showIssuesForStudent($library_render_wrapper->student, $issues_xml);
		else:
			$out = html_writer::tag('p', 'Athenaeum library does not appear to be accessible.');
		endif;
		
		return $out;
}
	
	protected function render_smslink_enrol(smslink_enrol $enrol_render_wrapper)
	{
		$enrolled_courses = array();
		foreach ($enrol_render_wrapper->enrolled_moodle_courses as $enrolled_moodle_course)
		{
			$enrolled_courses[] = html_writer::tag('p', 'Enrolled in Moodle course <strong>' . $enrolled_moodle_course . '</strong>');
		}
		$removed_courses = array();
		foreach ($enrol_render_wrapper->removed_moodle_courses as $removed_moodle_course)
		{
			$removed_courses[] = html_writer::tag('p', 'Removed from Moodle course <strong>' . $removed_moodle_course . '</strong>');
		}
		$failed_courses = array();
		foreach ($enrol_render_wrapper->failed_moodle_courses as $failed_moodle_course)
		{
			$failed_courses[] = html_writer::tag('p', 'Error: KAMAR subject <strong>' . $failed_moodle_course . '</strong> was not found in Moodle, could not enrol in this course');
		}
		// Display the content as a list
		$out = $this->output->container(html_writer::alist($enrolled_courses), '', '');
		$out .= $this->output->container(html_writer::alist($removed_courses), '', '');
		$out .= $this->output->container(html_writer::alist($failed_courses), '', '');
		
		return $out;
	}
}
