<?php
	
function safe_mail($from, $to, $subject, $text, $headers="")
{
	$mail_sep = PHP_EOL;
	
	function _rsc($s)
	{
		$s = str_replace("\n", '', $s);
		$s = str_replace("\r", '', $s);
		return $s;
	}

	$h = '';
	if (is_array($headers))
	{
		foreach($headers as $k=>$v)
			$h = _rsc($k).': '._rsc($v).$mail_sep;
		if ($h != '')
		{
			$h = substr($h, 0, strlen($h) - strlen($mail_sep));
			$h = $mail_sep.$h;
		}
	}

	$from = _rsc($from);
	$to = _rsc($to);
	$subject = _rsc($subject);
	mail($to, $subject, $text, 'From: '.$from.$h);
}

function get_student($username, $studentid)
{
	global $CFG;
	
	$student = null;
	try {
		$student = StudentFactory::buildStudentFromUsername($username, date('Y'));
	}
	catch (Exception $e) {
		if ($CFG->block_kamar_allow_fallback_id && !empty($studentid))
		{
			$student = StudentFactory::buildStudentFromID($studentid, date('Y'));
		}
		else
		{
			throw $e;
		}
	}
	
	return $student;
}

require_once('../config.php');
require_once('renderer.php');

$view = optional_param('view', 'notices', PARAM_ALPHAEXT);

// increase memory limit, if its not enough, due to the potentially overwhelming number of results for some queries
raise_memory_limit("32M");

$PAGE->set_context(context_system::instance());

$smslink_css = new moodle_url('/smslink/styles.css');
$PAGE->requires->css($smslink_css, true);

$current_url = new moodle_url('/smslink/index.php', array('view' => $view));
$PAGE->set_url($current_url);

$PAGE->set_title('SMS Link - ' . ucwords($view));
$PAGE->set_heading('SMS Link');
$PAGE->set_pagelayout('standard');
if ($view != 'nceagraph')
	echo $OUTPUT->header();
// moodle setup page stuff above

// KCI setup
global $CFG, $USER, $DB;
require_once($CFG->libdir."/kamar/kamarcommoninterface/kamar.php");
require_once($CFG->libdir."/kamar/kamarcommondisplay/kamar.php");

$kamar = Kamar::getKAMARInstance();
$kamar->setFMPath($CFG->block_kamar_server_address);
$kamar->setWebAPIPassword($CFG->block_kamar_web_api_password);

// setup the KAMAR write instance
if (in_array($view, array('courseselection', 'interviews'))) {
	$kamar_write = Kamar::getKAMARInstanceWithWrite();
	$kamar_write->setFMPath($CFG->block_kamar_server_address);
	$kamar_write->setWebAPIPassword($CFG->block_kamar_web_api_password);
}

$student_block_version = get_config('block_kamar_students', 'version');

$kamar_cache = KamarCache::getKAMARCacheInstance();
$kamar_cache->setCachePath($CFG->block_kamar_cache_directory, 'KAMARMoodle-' . $student_block_version);
$kcd_utilities = KamarKCDUtilities::getKamarKCDUtilitiesInstance();
$kcd_utilities->setBaseURL($CFG->wwwroot . '/smslink/');

// set the renderer to our smslink render
$OUTPUT = new mod_smslink_renderer($PAGE, 'general');

// check for connectivity		
if(!KamarUtility::testConnection())
	$view = 'unavailable';

// smslink content goes here
switch ($view)
{
	case 'notices':
		// grab the date we are after from the URL
		$selected_date = strtotime(optional_param('date', date("Y-m-d"), PARAM_ALPHANUMEXT));
	
		// query the KCI for a notice list
		$notice_list = NoticeFactory::buildWebNoticeList($selected_date);
		
		$notice_list_render_wrapper = new smslink_notices($selected_date, $notice_list);
		
		echo $OUTPUT->heading('Notices', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($notice_list_render_wrapper), 'smslinkbox smslink-noticelist');
	break;
	
	case 'calendar':
		$selected_date = strtotime(optional_param('date', date("Y-m-d"), PARAM_ALPHANUMEXT));
		$range = optional_param('range', 'month', PARAM_ALPHANUMEXT);
	
		// query the KCI for todays events
		$event_list = EventFactory::buildEventsForMonth($selected_date, true);;
		
		$event_list_render_wrapper = new smslink_events($selected_date, $event_list, $range);
		
		echo $OUTPUT->heading('Calendar', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($event_list_render_wrapper), 'smslinkbox smslink-eventlist');
	break;
	
	case 'details':
		// check for change of details request and process it if appropriate
		if(!empty($_POST) && isset($_POST['request_change']) && $_POST['request_change'] == 'request_change')
		{
			if(empty($_POST['details']) || empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone'])) {
				echo ('<div class="notifyproblem error">You need to fill out all parts of the form to request a change.</div>');
			} else {
				$change_details = $_POST['details'];
				$requested_by = $_POST['name'];
				$requested_email = $_POST['email'];
				$requested_phone = $_POST['phone'];
				$first_name = $USER->firstname;
				$last_name = $USER->lastname;
				$body = <<<EOT
Hello KAMAR Admin,

$requested_by has requested a change to the KAMAR details for $first_name $last_name

Please review the change and update their details if appropriate...

$change_details

If you need to follow up the change request you can contact $requested_by by email [$requested_email] or phone [$requested_phone].

From,
KAMAR->Moodle Integration
EOT;
				safe_mail("no-reply@my-moodle.org", $CFG->block_kamar_admin_email, "Change of Student Details for " . $USER->firstname . " " . $USER->lastname, $body);
				echo ('<div class="notifysuccess">Your change of details request has been sent.</div>');
			}
	
		}
	
		// query the KCI for the student
		$student = get_student($USER->username, $USER->idnumber);
		
		$post_url = new moodle_url('/smslink/index.php', array('view' => 'details', 'section' => 'change'));
		
		$details_render_wrapper = new smslink_details($student, $CFG->block_kamar_admin_email, $post_url);
		
		echo $OUTPUT->heading('Details', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($details_render_wrapper), 'smslinkbox smslink-details');
	break;
	
	case 'timetable':
		$selected_week = optional_param('week', '', PARAM_INT);
		
		// query the KCI for the selected weeks timetable
		$student = get_student($USER->username, $USER->idnumber);
		$student_timetable = TimetableFactory::buildStudentTimetableWeek($student, $selected_week, date('Y'));
		
		$current_week = !empty($selected_week) ? $selected_week : $student_timetable->week_number;
		
		$timetable_render_wrapper = new smslink_timetable($current_week, $student, $student_timetable);
		
		echo $OUTPUT->heading('Timetable', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($timetable_render_wrapper), 'smslinkbox smslink-timetable');
	break;
	
	case 'attendance':
		$selected_week = optional_param('week', '', PARAM_INT);
		
		// query the KCI for the selected weeks timetable and attendance data
		$student = get_student($USER->username, $USER->idnumber);
		$student_timetable = TimetableFactory::buildStudentTimetableWeek($student, $selected_week, date('Y'));
		$student_attendance = AttendanceFactory::buildStudentAttendanceWeek($student, $student_timetable->monday_date, date('Y'));
		
		$current_week = !empty($selected_week) ? $selected_week : $student_timetable->week_number;
		
		$attendance_delay = $CFG->block_kamar_attendance_delay;
		
		$codes_to_hide = array();
		if (!$CFG->block_kamar_attendance_show_present)
			$codes_to_hide[] = ATTENDANCE_CODE_PRESENT;
		if (!$CFG->block_kamar_attendance_show_late)
			$codes_to_hide[] = ATTENDANCE_CODE_LATE;
		if (!$CFG->block_kamar_attendance_show_unjustified)
			$codes_to_hide[] = ATTENDANCE_CODE_ABSENT_UNJUSTIFIED;
		if (!$CFG->block_kamar_attendance_show_justified)
			$codes_to_hide[] = ATTENDANCE_CODE_ABSENT_JUSTIFIED;
		if (!$CFG->block_kamar_attendance_show_holiday)
			$codes_to_hide[] = ATTENDANCE_CODE_OVERSEAS_HOLIDAY;
		
		$attendance_render_wrapper = new smslink_attendance($current_week, $student, $student_timetable, $student_attendance, $attendance_delay, $codes_to_hide);
		
		echo $OUTPUT->heading('Attendance', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($attendance_render_wrapper), 'smslinkbox smslink-attendance');
	break;
	
	case 'nzqa':
		// query the KCI for the students results
		$student = get_student($USER->username, $USER->idnumber);
		$student_result_list = ResultFactory::buildWebStudentResultList($student, true);
		$ncea_summary = ResultFactory::buildWebNCEASummary($student, date('Y'));
		
		$assessment_type_codes = array('A', 'U');
		$enrolled_only = true;
		$published_only = false;
		$graph_url = new moodle_url('/smslink/index.php', array('view' => 'nceagraph', 'student' => $student->student_id));
		
		$result_list_render_wrapper = new smslink_results_nzqa($student, $student_result_list, $assessment_type_codes, $ncea_summary, $enrolled_only, $published_only, $graph_url);
		
		echo $OUTPUT->heading('NZQA Results', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($result_list_render_wrapper), 'smslinkbox smslink-nzqa');
	break;
	
	case 'nceagraph':
		// query the KCI for the students results
		$student = get_student($USER->username, $USER->idnumber);
		$ncea_summary = ResultFactory::buildWebNCEASummary($student, date('Y'));
		
		$result_graph_render_wrapper = new smslink_results_nceagraph($student, $ncea_summary);
		
		echo $OUTPUT->render($result_graph_render_wrapper);
	break;
	
	case 'results':
		// query the KCI for the students results
		$student = get_student($USER->username, $USER->idnumber);
		$student_result_list = ResultFactory::buildWebStudentResultList($student, true);
		
        // convert config string to assessment types array
		$raw_assessment_type_codes = $CFG->block_kamar_show_assessment_types;
		$assessment_type_codes = array();
		for ($i = 0; $i < strlen($raw_assessment_type_codes); $i++)
			$assessment_type_codes[] = $raw_assessment_type_codes[$i];
        
    	// convert config string to assessment comment array
    	$raw_assessment_comment_codes = $CFG->block_kamar_show_assessment_comments;
    	$assessment_comment_codes = array();
    	for ($i = 0; $i < strlen($raw_assessment_comment_codes); $i++)
    		$assessment_comment_codes[] = $raw_assessment_comment_codes[$i];
        
		$enrolled_only = true;
		$published_only = $CFG->block_kamar_published_results;
		
		$year_to_show = date('Y');
		
		$result_list_render_wrapper = new smslink_results_subject($student, $student_result_list, $assessment_type_codes, $assessment_comment_codes, $enrolled_only, $published_only, $year_to_show);
		
		echo $OUTPUT->heading('Subject Results', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($result_list_render_wrapper), 'smslinkbox smslink-results');
	break;
	
	case 'allresults':
		// query the KCI for the students results
		$student = get_student($USER->username, $USER->idnumber);
		$student_result_list = ResultFactory::buildWebStudentResultList($student, true);
		
        // convert config string to assessment types array
		$raw_assessment_type_codes = $CFG->block_kamar_show_assessment_types;
		$assessment_type_codes = array();
		for ($i = 0; $i < strlen($raw_assessment_type_codes); $i++)
			$assessment_type_codes[] = $raw_assessment_type_codes[$i];
        
    	// convert config string to assessment comment array
    	$raw_assessment_comment_codes = $CFG->block_kamar_show_assessment_comments;
    	$assessment_comment_codes = array();
    	for ($i = 0; $i < strlen($raw_assessment_comment_codes); $i++)
    		$assessment_comment_codes[] = $raw_assessment_comment_codes[$i];
        
		$enrolled_only = true;
		$published_only = $CFG->block_kamar_published_results;
		
		$result_list_render_wrapper = new smslink_results_all($student, $student_result_list, $assessment_type_codes, $assessment_comment_codes, $enrolled_only, $published_only);
		
		echo $OUTPUT->heading('All Results', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($result_list_render_wrapper), 'smslinkbox smslink-results');
	break;
	
	case 'groups':
		// query the KCI for the students groups
		$student = get_student($USER->username, $USER->idnumber);
		$student_group_list = GroupFactory::buildWebStudentGroupList($student);
		
		$show_groups_from_year = $CFG->block_kamar_show_groups_from_year;
		
		$group_list_render_wrapper = new smslink_groups($student, $student_group_list, $show_groups_from_year);
		
		echo $OUTPUT->heading('Groups', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($group_list_render_wrapper), 'smslinkbox smslink-groups');
	break;
	
	case 'awards':
		// query the KCI for the students awards
		$student = get_student($USER->username, $USER->idnumber);
		$student_award_list = AwardFactory::buildWebStudentAwardList($student);
		
		$award_list_render_wrapper = new smslink_awards($student, $student_award_list);
		
		echo $OUTPUT->heading('Awards', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($award_list_render_wrapper), 'smslinkbox smslink-awards');
	break;
	
	case 'pastoral':
		// query the KCI for the students pastoral record
		$student = get_student($USER->username, $USER->idnumber);
		$student_pastoral_list = PastoralFactory::buildWebStudentPastoralList($student);
		
		$pastoral_list_render_wrapper = new smslink_pastoral($student, $student_pastoral_list);
		
		echo $OUTPUT->heading('Pastoral', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($pastoral_list_render_wrapper), 'smslinkbox smslink-pastoral');
	break;
	
	case 'reports':
		// query the KCI for the student
		$student = get_student($USER->username, $USER->idnumber);
		$reports_path = $CFG->block_kamar_reports_directory;
		
		$reports_render_wrapper = new smslink_reports($student, $reports_path);
		
		echo $OUTPUT->heading('Reports', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($reports_render_wrapper), 'smslinkbox smslink-reports');
	break;
	
	case 'fees':
		// query the KCI for the student
		$student = get_student($USER->username, $USER->idnumber);
		
		// query for the students funds records
		$fee_payer = null;
		$fee_summary = null;
		try {
			$fee_payer = FeeFactory::buildPayerFromPayerID($student->student_id);
			$fee_summary = FeeFactory::buildFeeSummary($student);
		} catch (Exception $e) {
		}
		
		$year_to_show = date('Y');
		
		$fees_render_wrapper = new smslink_fees($student, $fee_payer, $fee_summary, $year_to_show, $CFG->block_kamar_show_credit_balance, $CFG->block_kamar_show_carry_forward_charges);
		
		echo $OUTPUT->heading('Fees', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($fees_render_wrapper), 'smslinkbox smslink-reports');
	break;
	
	case 'careers':
		// query the KCI for the student
		$student = get_student($USER->username, $USER->idnumber);
		
		// get the careers headings via KCI
		$careers_headings = GlobalsFactory::getCareersHeadingsArray();
		
		$careers_fields_enabled = array();
		if($CFG->block_kamar_show_career_interests)
			$careers_fields_enabled[1] = true;
		else
			$careers_fields_enabled[1] = false;
		if($CFG->block_kamar_show_career_intentions)
			$careers_fields_enabled[2] = true;
		else
			$careers_fields_enabled[2] = false;
		if($CFG->block_kamar_show_career_experience)
			$careers_fields_enabled[3] = true;
		else
			$careers_fields_enabled[3] = false;
		if($CFG->block_kamar_show_career_events)
			$careers_fields_enabled[4] = true;
		else
			$careers_fields_enabled[4] = false;
		if($CFG->block_kamar_show_career_notes)
			$careers_fields_enabled[5] = true;
		else
			$careers_fields_enabled[5] = false;
		
		$careers_render_wrapper = new smslink_careers($student, $careers_fields_enabled, $careers_headings);
		
		echo $OUTPUT->heading('Careers', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($careers_render_wrapper), 'smslinkbox smslink-reports');
	break;
	
	case 'interviews':
		// query the KCI for the student and interviews
		$student = get_student($USER->username, $USER->idnumber);
		$interviewlist = CareersFactory::buildWebStudentInterviewList($student);
		
		// grab the current viewing interview id from the URL parameter
		$viewing_interview_id = optional_param('viewing', null, PARAM_INT);
		if ($viewing_interview_id === null && !empty($interviewlist) && !empty($interviewlist->interviews))
			$viewing_interview_id = $interviewlist->interviews[0]->interview_id;
		
		// grab the current interview itself
		$viewing_interview = null;
		foreach ($interviewlist->interviews as $interview)
		{
			if ($interview->interview_id == $viewing_interview_id)
				$viewing_interview = $interview;
		}
	
		// grab answers and check if its a read only interview
		$viewing_interview_is_read_only = true;
		$viewing_interview_answer_list = null;
		if ($viewing_interview !== null)
		{
			// if the viewing interview is still in progress and the interview is the latest one (first in list) then allow it to be edited
			if (!$viewing_interview->interview_completed && $interviewlist->interviews[0]->interview_id == $viewing_interview->interview_id)
				$viewing_interview_is_read_only = false;
	
			// pull the interview answers
			$viewing_interview_answer_list = $viewing_interview->getAnswerList();
		}
		
		// check for a change in interview answers and process it if appropriate
		if (!empty($_POST) && $viewing_interview_answer_list !== null && !empty($viewing_interview_answer_list))
		{
			foreach ($viewing_interview_answer_list->answers as $interview_answer)
			{
				if (array_key_exists('interview-answer-' . $interview_answer->question_id, $_POST))
					$interview_answer->answer_text = $_POST['interview-answer-' . $interview_answer->question_id];
			}
		
			if (!$viewing_interview_is_read_only && $viewing_interview->save())
				echo ('<div class="notifysuccess">Your answers have been saved</div>');
			else
				echo ('<div class="notifyproblem error">There was a problem saving your answers, please try again and if the issue continues contact the school</div>');
		}
		
		$post_url = new moodle_url('/smslink/index.php', array('view' => 'interviews', 'viewing' => $viewing_interview_id));
		
		$interviews_render_wrapper = new smslink_interviews($student, $interviewlist, $viewing_interview, $viewing_interview_answer_list, $viewing_interview_is_read_only, $post_url);
		
		echo $OUTPUT->heading('Interviews', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($interviews_render_wrapper), 'smslinkbox smslink-reports');
	break;
	
	case 'courseselection':
		// query the KCI for the student
		$student = get_student($USER->username, $USER->idnumber);
		
		$course_selection_file_path = $CFG->block_kamar_course_selection_path;
		
		clearstatcache();
		$course_selection_xml = false;
		if (is_readable($course_selection_file_path))
			$course_selection_xml = simplexml_load_file($course_selection_file_path);
		
		$selection_year = date('Y', strtotime('+1 year'));
		$selection_year_options = $student->getStudentOptionsForYear($selection_year);
	
		if (!empty($_POST))
		{
			$new_alternative_subjects = array();
			foreach ($_POST as $key=>$value)
			{
				// check we are getting what we expect for a subject update
				if (substr($key, 0, 5) == 'slot-' && $value != 'none')
				{
					// identify which line is being updated
					$line_index = substr($key, 5);
					// build a special array for alternative subjects
					if (substr($line_index, 0, 6) == 'ALTSUB')
						$new_alternative_subjects[] = $value;
					else
						$selection_year_options->line_subjects[$line_index] = $value;
				}
			}
			// only update if the alternatives were not locked
			if (!empty($new_alternative_subjects))
				$selection_year_options->alternative_subjects = $new_alternative_subjects;
		
			if ($selection_year_options->save())
				$redirect_url = new moodle_url('/smslink/index.php', array('view' => 'courseselection', 'status' => 'success'));
			else
				$redirect_url = new moodle_url('/smslink/index.php', array('view' => 'courseselection', 'status' => 'error'));
			
			redirect($redirect_url);
		}
		
		$url_status = optional_param('status', '', PARAM_ALPHANUMEXT);
		
		$post_url = new moodle_url('/smslink/index.php', array('view' => 'courseselection'));
		
		$course_selection_render_wrapper = new smslink_course_selection($student, $selection_year, $selection_year_options, $course_selection_xml, $url_status, $post_url);
		
		echo $OUTPUT->heading('Course Selection', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($course_selection_render_wrapper), 'smslinkbox smslink-reports');
	break;
	
	case 'library':
		// query the KCI for the student
		$student = get_student($USER->username, $USER->idnumber);
		
		$userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322) KAMARWeb';
		$target_url = 'http://' . $CFG->block_kamar_server_address . '/fmi/xml/FMPXMLRESULT.xml?-db=ap_web&-lay=circulation&circ_barCodeBorrower=' . $student->student_id . '&circ_barCodeBorrower.op=eq&-find&-lay.response=circulation';
		$athenaeum_username_password = 'kamarxml:xml4adam';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_URL,$target_url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERPWD, $athenaeum_username_password);
		$raw_xml = curl_exec($ch);
		$valid_query = true;
		if ($raw_xml === FALSE)
			$valid_query = false;
		
		$library_render_wrapper = new smslink_library($student, $raw_xml, $valid_query);
		
		echo $OUTPUT->heading('Library - Issues', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($library_render_wrapper), 'smslinkbox smslink-reports');
	break;
	
	case 'enrol':
		// query the KCI for the student and their classes
		$student = get_student($USER->username, $USER->idnumber);
		$student_class_list = ClassFactory::buildStudentClassesList($student, date('Y'));
		$student_role = $DB->get_record('role', array('shortname' => 'student')); // Return Internal ID
		// grab a reference to the kamar enrollment plugin
		if (!$enrol_kamar = enrol_get_plugin('kamar')) {
		    throw new coding_exception('Can not instantiate enrol_kamar');
		}
		
		// enrol the student in new courses
		$failed_moodle_courses = array();
		$enrolled_moodle_courses = array();
		foreach ($student_class_list->classes as $student_class)
		{
			$course_to_enrol_into = null;
			// check if tt_code represents a course
			$course_details = $DB->get_record('course', array('shortname' => $student_class->tt_code));
			// check if there was even a record returned
			if (empty($course_details))
			{
				$failed_moodle_courses[] = $student_class->tt_code;
			}
			else
			{
				// if the course isn't a meta course
				if ($course_details->metacourse == 0)
				{
					$course_to_enrol_into = $course_details;
				}
				// is a meta course, need to find the child course
				else
				{
					// confirm if a child course exists
					$child_course_details = $DB->get_record('course', array('shortname' => $student_class->tt_code . '-' . $student_class->line_identifier));
					if (empty($child_course_details))
					{
						$failed_moodle_courses[] = $student_class->tt_code . '-' . $student_class->line_identifier;
					}
					else
					{
						$course_to_enrol_into = $child_course_details;
					}
				}
			}
		
			if (!empty($course_to_enrol_into))
			{
				$course_context = get_context_instance(CONTEXT_COURSE, $course_to_enrol_into->id); // Return Context ID from Course ID
				
				// grab enrollment instance for course, or create a new one
                if (!$instance = $DB->get_record('enrol', array('courseid'=>$course_to_enrol_into->id, 'enrol'=>'kamar'), '*', IGNORE_MULTIPLE)) {
					$instance = $enrol_kamar->add_instance($course_to_enrol_into);
                }
				
				if (!$instance = $DB->get_record('enrol', array('courseid'=>$course_to_enrol_into->id, 'enrol'=>'kamar'), '*', IGNORE_MULTIPLE)) {
					// world is broken
				    throw new coding_exception('Moodle not accepting new enrollment instances from enrol_kamar');
				}
				
				$enrol_kamar->enrol_user($instance, $USER->id, $student_role->id); // Enrol
				$enrolled_moodle_courses[] = $course_to_enrol_into->shortname;
			}
		}
		
		// remove the student from old courses
		$removed_moodle_courses = array();
		$existing_course_roles = $DB->get_records('role_assignments', array('userid' => $USER->id));
		$enrolled_moodle_courses_flipped = array_flip($enrolled_moodle_courses);
		if (!empty($existing_course_roles))
		{
			foreach ($existing_course_roles as $existing_course_role)
			{
				// make sures its a course we are managing
				if ($existing_course_role->component == 'enrol_kamar')
				{
					$existing_course_context = get_context_instance_by_id($existing_course_role->contextid);
					$existing_course = $DB->get_record('course', array('id' => $existing_course_context->instanceid));
					// student is no longer in this course within KAMAR
					if (!array_key_exists($existing_course->shortname, $enrolled_moodle_courses_flipped))
					{
						// grab enrollment instance for course
						if (!$instance = $DB->get_record('enrol', array('courseid'=>$existing_course->id, 'enrol'=>'kamar'), '*', IGNORE_MULTIPLE)) {
							// world is broken
						    throw new coding_exception('Moodle not accepting new enrollment instances from enrol_kamar');
						}
						
						$enrol_kamar->unenrol_user($instance, $USER->id, $student_role->id); // Un-Enrol
						$removed_moodle_courses[] = $existing_course->shortname;
					}
				}
			}
		}
		
		$enrol_render_wrapper = new smslink_enrol($student, $enrolled_moodle_courses, $removed_moodle_courses, $failed_moodle_courses);
		
		echo $OUTPUT->heading('Enrollment', 2, 'headingblock header');
		echo $OUTPUT->box($OUTPUT->render($enrol_render_wrapper), 'smslinkbox smslink-enrol');
	break;
	
	case 'unavailable':
		echo ('There was a problem with the SMS Link. Please try again later and if this message persists, contact the school');
	break;
	
	default:
		echo ('invalid view specified');
	break;
}

// moodle setup page stuff below
echo $OUTPUT->footer();
