<?php

$plugin->version = 2014060700;
$plugin->cron = 0;
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '2.x (Build: 2014060700)';
$plugin->component = 'block_objectives';
$plugin->requires = 2010112400; // Moodle 2.0