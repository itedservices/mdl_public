<?php

	defined('MOODLE_INTERNAL') || die();

	$module->version   = 2015021301;       // The current module version (Date: YYYYMMDDXX)
	$module->requires  = 2012062500;       // Requires this Moodle version
	$module->component = 'mod_clickview';  // Full name of the plugin (used for diagnostics)
	$module->cron      = 0;
