<?php

	defined('MOODLE_INTERNAL') || die();

	function clickview_add_instance($clickview)
	{
		global $DB;

		$clickview->width = $clickview->clickview['width'];
		$clickview->height = $clickview->clickview['height'];
		$clickview->shortcode = $clickview->clickview['embedid'];
		$clickview->autoplay = $clickview->clickview['autoplay'];
		
		$clickview->timemodified = time();
		
		return $DB->insert_record('clickview', $clickview);
	}

	function clickview_update_instance($clickview)
	{
		global $DB;
		
		$clickview->width = $clickview->clickview['width'];
		$clickview->height = $clickview->clickview['height'];
		$clickview->shortcode = $clickview->clickview['embedid'];
		$clickview->autoplay = $clickview->clickview['autoplay'];
		
		$clickview->timemodified = time();
		
		$clickview->id = $clickview->instance;

		return $DB->update_record('clickview', $clickview);
	}

	function clickview_delete_instance($id)
	{
		global $DB;

		if (! $clickview = $DB->get_record("clickview", array("id"=>$id))) {
			return false;
		}

		$result = true;

		if (! $DB->delete_records("clickview", array("id"=>$clickview->id))) {
			$result = false;
		}

		return $result;
	}

	function clickview_supports($feature) {
		switch($feature) {
			case FEATURE_MOD_ARCHETYPE: return MOD_ARCHETYPE_RESOURCE;
			case FEATURE_MOD_INTRO: return false;
			default: return null;
		}
	}