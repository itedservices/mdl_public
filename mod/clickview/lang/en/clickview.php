<?php

//Clickview Selector form
$string['editor:title'] = 'Display Title';
$string['editor:selector'] = 'Select Video';
$string['editor:required'] = 'Required';
$string['editor:selectorerror'] = 'Please select a video';

//Clickview Selector Frame
$string['selector:title'] = "Insert a ClickView video";

//Moodle Internals
$string['modulename'] = 'ClickView Video';
$string['modulename_help'] = 'The ClickView Video module allows you to search for and embed any ClickView video into your course page.

The ClickView Video module enables you to:

* Embed a video from your School Library
* Embed a video from The ClickView Exchange
* Embed a video from your ClickView Workspace';
$string['modulename_link'] = 'mod/clickview/view';
$string['modulenameplural'] = 'ClickView Videos';
$string['pluginadministration'] = 'ClickView Administration';
$string['pluginname'] = 'Clickview Video';