<?php

global $CFG;

require_once($CFG->libdir.'/pear/HTML/QuickForm/element.php');

class MoodleQuickForm_clickview_selector extends HTML_QuickForm_element {

	private $_name;
	private $_values = array('width'=>null, 'height'=>null, 'embedid'=>null, 'autoplay'=>null);

	function MoodleQuickForm_clickview_selector($elementName=null, $elementLabel=null, $attributes=null, $options=null) {
		parent::HTML_QuickForm_element($elementName, $elementLabel, $attributes);
	}

	function getFrozenHtml() {
		return '';
	}

	function setName($name) {
		$this->_name = $name;
	}

	function getName() {
		return $this->_name;
	}

	function setValue($values) {
		$values = (array)$values;
		foreach ($values as $name=>$value) {
			if (array_key_exists($name, $this->_values)) {
				$this->_values[$name] = $value;
			}
		}
	}

	function getValue() {
		return $this->_values;
	}

	function toHtml() {
		require_once(dirname(dirname(__FILE__)).'/cv-config.php');
		require_once(dirname(dirname(__FILE__)).'/schoolId.php');
		
		$elname = $this->getName();

		$param = '';

		if(! empty($SCHOOL_ID)) {
			$param = '&schoolId=' . $SCHOOL_ID->value;
		}
		
		$str = 	'<iframe id="cv-plugin-frame" frameborder="0" src="'.$CFG_CLICKVIEW->pluginFrameUrl.'&singleSelectMode=true'.$param.'" width="'.$CFG_CLICKVIEW->iframeWidth.'" height="'.$CFG_CLICKVIEW->iframeHeight.'" ></iframe>'.
				'<input name="'.$elname.'[width]" id="width" type="text" style="display: none;" />'.
				'<input name="'.$elname.'[height]" id="height" type="text" style="display: none;" />'.
				'<input name="'.$elname.'[embedid]" id="embedid" type="text" style="display: none;" />'.
				'<input name="'.$elname.'[autoplay]" id="autoplay" type="text" style="display: none;" />';

		return $str;
	}
}
