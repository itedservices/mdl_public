(function () {
	var pluginFrame = document.getElementById('cv-plugin-frame')
	var eventsApi = new CVEventsApi(pluginFrame.contentWindow);
	eventsApi.on('cv-lms-addvideo', function (event, detail) {
		document.getElementById('width').value = detail.embed.width;
		document.getElementById('height').value = detail.embed.height;
		document.getElementById('embedid').value = detail.shortCode;
		document.getElementById('autoplay').value = (detail.embed.autoplay ? '1' : '0');
	}, true);
})();
