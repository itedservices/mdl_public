<?php
	require_once(dirname(__FILE__).'/schoolId.php');

	$param = '';

	if(! empty($SCHOOL_ID)) {
		$param = '&schoolId=' . $SCHOOL_ID->value;
	}
	
	unset($CFG_CLICKVIEW);
	
	$CFG_CLICKVIEW =  new stdClass();
	$CFG_CLICKVIEW->onlineHost = "https://online.clickview.co.nz";
	$CFG_CLICKVIEW->consumerKey = "c6c10e9df125";
	$CFG_CLICKVIEW->pluginFrameUrl =  $CFG_CLICKVIEW->onlineHost."/lmsplugin?consumerKey=".$CFG_CLICKVIEW->consumerKey.$param;
