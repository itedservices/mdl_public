<?php

defined('MOODLE_INTERNAL') || die();

	// The current plugin version (Date: YYYYMMDDXX).
	$plugin->version   = 2015021301;
	// Required Moodle version.
	$plugin->requires  = 2012120300;
	// Full name of the plugin (used for diagnostics).
	$plugin->component = 'tinymce_clickviewembed';
