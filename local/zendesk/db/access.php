<?php
defined('MOODLE_INTERNAL') || die();

$capabilities = array(
 
    'local/zendesk:viewwidget' => array(
        
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
        )
    )
);
