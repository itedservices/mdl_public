<?php

defined('MOODLE_INTERNAL') || die;

if (!isloggedin()) {
    return;
}

function load_widget() {
        
    global $USER, $CFG, $PAGE;
    
    $config = new stdClass;
    $config->domain = get_config('zendesk', 'domain');
    
    // If we don't have a domain set, then don't continue
    if (!$config->domain) {
        return;
    }

    $config->user = new stdClass;

    $config->user->id    = $USER->id;
    $config->user->email = $USER->email;
    $config->user->name  = "$USER->firstname " . "$USER->lastname";

    // Only require JS if we have the capability and if user is not a 
    // siteadmin, only require if show_for_teacher is true
    $allowteacher  = get_config('zendesk', 'show_for_teacher');
    $hascapability = has_capability('local/zendesk:viewwidget', $PAGE->context);
    
    if ($allowteacher && $hascapability) {
        get_widget($config);
        
    } else if (is_siteadmin($USER->id) && $hascapability) {
        get_widget($config);
    
    }
}

function get_widget($config) {
    
    global $PAGE;
    
    $PAGE->requires->yui_module('moodle-local_zendesk-zendesk', 'M.local_zendesk.zendesk.init', array($config), null, true);
}

load_widget();
