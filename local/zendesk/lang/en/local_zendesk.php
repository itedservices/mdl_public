<?php

$string['pluginname'] = 'Zendesk Widget';

$string['zendesk:viewwidget'] = 'Allow user to access the widget.';

$string['config']      = 'Widget Configuration';
$string['config_desc'] = 'The Web Widget embeds Zendesk functionality such as ticketing, live chat, and Help Center KB searches into Moodle.\nThe only configuration required here is your Zendesk domain. The rest comes from the Widget settings from within Zendesk admin.';

$string['zd_domain']      = 'Zendesk Domain';
$string['zd_domain_desc'] = 'This is your Zendesk domain name. e.g. support.zendesk.com';

$string['teacherrole'] = 'Show for Teachers';
$string['teacherrole_desc'] = 'Shows the widget for users with the Teacher role (editingteacher) in the current context';
