<?php
defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) { // needs this condition or there is error on login page
    $settings = new admin_settingpage(
        'local_zendesk', 
        get_string('pluginname', 'local_zendesk')
    );
    
    $ADMIN->add('localplugins', $settings);

    $settings->add(
        new admin_setting_heading(
            'zendesk/headerconfig',
            get_string('config', 'local_zendesk'),
            get_string('config_desc', 'local_zendesk')
    ));
    
    $settings->add(
        new admin_setting_configtext(
            'zendesk/domain',
            get_string('zd_domain', 'local_zendesk'),
            get_string('zd_domain_desc', 'local_zendesk'),
            null
    ));
    
    $settings->add(new admin_setting_configcheckbox(
            'zendesk/show_for_teacher',
            get_string('teacherrole', 'local_zendesk'), 
            get_string('teacherrole_desc', 'local_zendesk'),
            '0'
    ));
    
}

