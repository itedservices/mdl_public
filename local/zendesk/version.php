<?php

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2015040200;
$plugin->requires  = 2010021900;
$plugin->maturity  = MATURITY_STABLE;
$plugin->component = 'local_zendesk';
